class Usuario
  attr_accessor :nome, :email

  def initialize(attributes = {})
    @nome  = attributes[:nome]
    @email = attributes[:email]
  end

  def formatted_email
    "#{@nome} <#{@email}>"
  end
end