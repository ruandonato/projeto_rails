Rails.application.routes.draw do
  resources :users

  get 'users/new'

  get 'usuario/new'
  resources :relatorios

  resources :produtos

  resources :funcionarios
  
  get  'welcome/about'
  

  root 'welcome#index'

end
