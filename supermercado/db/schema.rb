# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141125120253) do

  create_table "funcionarios", force: true do |t|
    t.string   "nome"
    t.string   "sexo"
    t.string   "email"
    t.string   "data"
    t.integer  "idade"
    t.integer  "salario"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "produtos", force: true do |t|
    t.string   "nome"
    t.string   "validade"
    t.string   "marca"
    t.string   "peso"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "relatorios", force: true do |t|
    t.text     "quantidade"
    t.integer  "produto_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "nome"
    t.string   "email"
    t.string   "password"
    t.boolean  "admin"
    t.boolean  "funcionario"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "nome"
    t.string   "email"
    t.boolean  "funcionario"
    t.boolean  "admin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
