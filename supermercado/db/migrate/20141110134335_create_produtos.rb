class CreateProdutos < ActiveRecord::Migration
  def change
    create_table :produtos do |t|
      t.string :nome
      t.string :validade
      t.string :marca
      t.string :peso

      t.timestamps
    end
  end
end
