class CreateRelatorios < ActiveRecord::Migration
  def change
    create_table :relatorios do |t|
      t.text :quantidade
      t.integer :produto_id

      t.timestamps
    end
  end
end
