json.array!(@relatorios) do |relatorio|
  json.extract! relatorio, :id, :quantidade, :produto_id
  json.url relatorio_url(relatorio, format: :json)
end
