json.array!(@users) do |user|
  json.extract! user, :id, :nome, :email, :password, :admin, :funcionario
  json.url user_url(user, format: :json)
end
