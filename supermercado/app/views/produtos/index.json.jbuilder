json.array!(@produtos) do |produto|
  json.extract! produto, :id, :nome, :validade, :marca, :peso
  json.url produto_url(produto, format: :json)
end
