class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def bemvindo
    render text: "Bem Vindo ao Sistema de Cadastro de Supermercado"
  end
end
