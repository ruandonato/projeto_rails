class SessionsController < ApplicationController

    def new
    end

    def create 
        user = User.find_by_email_user(params[:session][:email_user]).downcase
        if user && user.authenticate(params[:session][:password])
            sign_in user
            redirect_to user
            flash[:sucesso] = "Logado com exito"
            
        else
            flash.now[:error] = "Combinacao invalida de email e password"
            
        end
        
    end
    
    #Sign out
    def destroy
        flash[:sucess] = "Deslogado com exito"
        sign_out
        redirec_to root_url
    end
    
end
